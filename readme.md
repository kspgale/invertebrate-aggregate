## Combine Invertebrate Data into a Presence-Only Dataset

Katie Gale, 2 Feb 2018

**ShellfishBio_Combine.R**
 - Combine and standardize data from the shellfish biological survey databases into a single presence-only file (NOT presence-absence)

**AggregateInvertData_MPA.R**
 - Combine the standardized shellfish bio data with various other relevant sources into a presence-only file (NOT presence-absence)
 - Can subset for just species of interest

### Datasets included
- Shellfish Biological Surveys (SFBio)
  - Red Sea Urchin Dive (RSUBio)
  - Green Sea Urchin Dive (GSUBio)
  - Sea Cucumber Dive (cukeBio)
  - Geoduck Dive (geoduckBio)
  - Intertidal Clam (clamBio)
  - Crab Trap (crabBio)
  - Prawn Trap (prawnBio)
  - Shrimp Trawl (shrimpBio)
  - Scallop Trawl (scallopTrawlBio)
  - Tanner Crab Survey (tannerBio)

- Shellfish Commercial Logbooks (SFLog)
  - Octopus bycatch from CrabLog (OctoCrabLog)
  - Octopus bycatch from PrawnLog (OctoPrawnLog)
  - Shrimp by trawl (shrimplog)
  - Sea Cucumber by dive (cukelog)
  - Crab by trap (crablog)
  - Squid by trawl (squidlog)
  - Red Sea Urchin by dive (rsulog)
  - Prawn by trap (prawnlog)
  - Geoduck by dive (geoducklog)
  - Clam by hand (clamlog)
  - Green Sea Uchin by dive (gsulog)
  - Tanner Crab experimental fishery (tannerlog)
  - Scallop by trawl (scalloptrawl)
  - Octopus by dive (octodivelog)

- Groundfish Biological Surveys (GFBio
  - Synoptic Trawl Survey (GFSynoptic)
  - JOINT CAN/US HAKE ACOUSTIC SURVEY
  - INSHORE ROCKFISH LONGLINE SURVEY
  - PHMA LONGLINE SURVEYS
  - STRAIT OF GEORGIA SYNOPTIC TRAWL SURVEY
  - IPHC LONGLINE SURVEY
  - HECATE STRAIT PCOD MONITORING TRAWL SURVEY
  - HECATE STRAIT MULTISPECIES TRAWL SURVEY
  - WEST COAST VANCOUVER ISLAND THORNYHEAD TRAWL SURVEY
  - SABLEFISH RESEARCH AND ASSESSMENT SURVEY
  - YELLOWEYE ROCKFISH CHARTER LONGLINE SURVEY
  - GEORGIA STRAIT ECOSYSTEM RESOURCE INITIATIVE (ERI) SURVEY
  - HAKE STOCK DELINEATION
  - High Seas Salmon Survey (HighSeas)

- Other
  - World Class Dive Surveys (WCDive) 
	- Start locations only, not "spatialized points"
  - World Class ROV Surveys (WCROV)